import keras
import numpy
import pandas
from matplotlib import pyplot
from keras.utils import plot_model
from keras.layers import Conv1D, GlobalMaxPool1D, Dropout, concatenate, Dense, Embedding, Input
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split

mx_len = 100

input_layer = Input(shape=(mx_len,))
current_layer = input_layer
current_layer = Embedding(20000, 200)(current_layer)
current_layer = Dropout(0.25)(current_layer)
current_layer = Conv1D(2 * 200,
                       kernel_size=3)(current_layer)
convolution_layers = []

sz = 32
for i in range(3):
    convolution_layers += [Dropout(0.5)(GlobalMaxPool1D()(Conv1D(sz, kernel_size=3, dilation_rate=i + 1)(current_layer)))]
    sz *= 2

current_layer = concatenate(convolution_layers)

current_layer = Dense(1, activation="tanh")(current_layer)

nn = keras.models.Model(inputs=input_layer, outputs=current_layer)
nn.compile(loss='mse', optimizer='adam', metrics=['mean_absolute_error'])

def preprocess_data(file):
    global mean_likes, standart_deviation
    data = pandas.read_csv(file)
    data['len_text'] = data['text'].str.len()
    data['likes'] = data['likes'].apply(lambda raw: int(float(raw[:-1]) * 1000) if raw[-1] == 'K' else int(raw))

    bound = numpy.percentile(data['likes'], 85)
    cutted = numpy.clip(data['likes'], 0, bound)
    mean_likes = numpy.mean(cutted)
    standart_deviation = data['likes'].std()
    data['likes'] = cutted
    data['like_zscore'] = ((cutted - mean_likes) / standart_deviation)

    return train_test_split(data, random_state=2234, test_size=0.3, stratify=pandas.qcut(cutted.values, 10, duplicates='drop'))


train_data, test_data = preprocess_data("articles.csv")

train_y = train_data['like_zscore']
test_y = test_data['like_zscore']

tokenizer = keras.preprocessing.text.Tokenizer(num_words=30000)
tokenizer.fit_on_texts(list(train_data['text'].values))

train_x = keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences(train_data['text'].values), maxlen=mx_len)
test_x = keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences(test_data['text'].values), maxlen=mx_len)

plot_model(nn, to_file="model.png", show_shapes=True, show_layer_names=True)

nn.fit(train_x, train_y, batch_size=32, epochs=30, validation_split=0.2)

test_data['pred_likes'] = nn.predict(test_x) * standart_deviation + mean_likes

error = mean_absolute_error(test_data['likes'], test_data['pred_likes'])

print('mean_absolute_error = ' + str(error))

f, x = pyplot.subplots(1, 1, figsize = (10, 10))
x.plot(test_data['likes'], test_data['pred_likes'], 'ro', label ='Predicted')
x.plot(test_data['likes'], test_data['likes'], 'b-', label ='Real')
x.legend()
f.savefig("result.png")
pyplot.close(f)